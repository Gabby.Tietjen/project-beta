from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    technician_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.technician_name

    def get_api_url(self):
        return reverse("technician", kwargs={"id": self.id})


class Appointment(models.Model):
    customer_name = models.CharField(max_length=200, null=True, blank=True)
    date = models.CharField(max_length=100, null=True, blank=True)
    time = models.CharField(max_length=100, null=True, blank=True)
    vin = models.CharField(max_length=17)
    reason = models.CharField(max_length=400, null=True, blank=True)
    vip_status = models.BooleanField(default=False, null=True, blank=True)
    appointment_status = models.CharField(default="APPROVED", max_length=10)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.customer_name
