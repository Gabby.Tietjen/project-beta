from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .encoders import (
    AutomobileVO,
    Technician,
    Appointment,
    TechnicianListEncoder,
    AppointmentListEncoder,
)
from django.http import JsonResponse
import json


@require_http_methods(["GET", "POST"])
def api_appointment_list(request, automobile_vin=None):
    if request.method == "GET":
        if automobile_vin == None:
            appointments = Appointment.objects.all()
        else:
            vin = automobile_vin
            appointments = Appointment.objects.filter(vin=vin)
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = content["technician"]
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=404,
            )
        vin = content["vin"]
        autos = AutomobileVO.objects.all()
        automobile_vin = []
        for auto in autos:
            automobile_vin.append(auto.vin)
        if vin in automobile_vin:
            content["vip_status"] = True
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse({"technician": technician}, encoder=TechnicianListEncoder)
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create Technician"})
            response.status_code = 400
            return response
