import React, {useState, useEffect} from 'react';


function NewAutoForm () {
  const [model, setmodel] = useState("");
  const [vin, setvin] = useState("");
  const [year, setyear] = useState("");
  const [modelList, setmodelList] = useState([]);
  const [color, setcolor] = useState("");
  

  const fetchData = async () => {
    const modelUrl = 'http://localhost:8100/api/models/';
    const modelResponse = await fetch(modelUrl);

    if (modelResponse.ok) {
      const data = await modelResponse.json();
      setmodelList(data.models);
    }
  }

  

  useEffect(()=> {
    fetchData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.color = color;
    data.vin = vin;
    data.year = Number(year);
    data.model_id = Number(model);

    const locationUrl = 'http://localhost:8100/api/automobiles/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setmodel('');
      setvin('');
      setyear('');
      setcolor('');
      alert("Car Added")
    }
    else {
        alert("Car Not added")
    }
  }
  
  return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Automobile</h1>
            <form onSubmit={handleSubmit} id="create-A-Car">
              <div className="form-floating mb-3">
                <select value={model} id="model" onChange={(event)=>setmodel(event.target.value)} name="name" className="form-control"> 
                <option value="">Choose an model</option>
                {modelList.map(model=>{
                    return <option value={model.id} key={model.id}>{model.year} {model.color} {model.manufacturer.name} {model.name}</option>
                })}
                </select>
                <label htmlFor="model">model</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={(event)=>setcolor(event.target.value)} placeholder="color" required type="text"  name="color" id="color" className="form-control" value={color}/>
                <label htmlFor="color">color</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={(event)=>setyear(event.target.value)} placeholder="year" required type="text"  name="year" id="year" className="form-control" value={year}/>
                <label htmlFor="year">year</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={(event)=>setvin(event.target.value)} placeholder="vin" required type="text"  name="vin" id="vin" className="form-control" value={vin}/>
                <label htmlFor="vin">Vin</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    
            );
        }

export default NewAutoForm;