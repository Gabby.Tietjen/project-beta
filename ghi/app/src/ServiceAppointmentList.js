import React, { useState, useEffect } from "react";


const ServiceAppointmentList = () => {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const appointmentResponse = await fetch(appointmentUrl);

        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const autoResponse = await fetch(autoUrl);

        if (appointmentResponse.ok) {
            const data = await appointmentResponse.json();
            setAppointments(data.appointments.filter(appoint => appoint.appointment_status === "APPROVED"));
        }

        if (autoResponse.ok) {
            const autodata = await autoResponse.json();
            setAutos(autodata.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const isVip = (vin) => {
        for (let auto of autos) {
            if (auto.vin === vin) {
                return true;
            } 
        }
        return false;
    }

    const handleFinished = async (id) => {
            const response = await
        fetch (`http://localhost:8080/api/appointments/${id}/`, {
            method: 'PUT', 
            body: JSON.stringify({appointment_status: "FINISHED"})},
        );
        if (response.ok) {
            fetchData();
        }
        alert("Service Appointment Finished!");
    }

    const handleDelete = async (id) => {
        try {
            const response = await
        fetch (`http://localhost:8080/api/appointments/${id}/`, {method: 'DELETE',});
        if (response.ok) {
            fetchData();
        }
        } catch (error) {
            console.error(error);
        } 
        alert("Service Appointment Canceled!");
    }
    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Dealership Purchased</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        const vipStatus = isVip(appointment.vin);
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.technician_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{vipStatus ? "yes" : "no"}</td> 

                                <td>
                                    <button onClick={() =>
                                    handleDelete(appointment.id)} className= "btn btn-danger">Cancel</button>
                                    <button onClick={() => 
                                    handleFinished(appointment.id)} className= "btn btn-success">Finished</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceAppointmentList