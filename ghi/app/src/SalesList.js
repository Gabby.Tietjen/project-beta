import React, {useState, useEffect} from "react";


function SalesList(props) {
    const [sales, setSales] = useState([])

    useEffect(()=>{
        const getFetch = async () => {
            const url = 'http://localhost:8090/api/sales/';
            const response = await fetch(url);
            const data = await response.json()
            setSales(data.sales)
        }
        getFetch()
    }, []);

    const deleteSale=async (id) => {
        const response= await fetch(`http://localhost:8090/api/sales/${id}`, {
            method: 'DELETE',
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            }
        })
        

            setSales(
            sales.filter((sale) =>{
              return sale.id !== id;
            })
            )
            alert("Deleted")
    }
 
  return (
    <>

      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
            <th>Sales Person</th>
                <th>Employee Number</th>
                <th>Customer</th>
                <th>Vin</th>
                <th>Price</th>
                <th></th>
            </tr>
          </thead>
          <tbody>
            {sales.map(sale => {
              return (
                <tr key={sale.id}>
                <td>{sale.sales_person.name}</td>
                  <td>{sale.sales_person.employee_number}</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>{sale.price}</td>

                    <td>
                        <button onClick={() => deleteSale(sale.id)}>Delete </button>
                    </td>

                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default SalesList;